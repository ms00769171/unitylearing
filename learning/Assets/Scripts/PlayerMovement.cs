﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public Rigidbody rb;

    public float forwardForce = 400;
    public float sidewayForce = 500;
    // Start is called before the first frame update
    void Start () {
        Debug.Log ("Hello");
        // rb.useGravity = false;
        // rb.AddForce(0,200,500);

    }

    // Update is called once per frame
    void Update () {

    }
    private void FixedUpdate () {
        rb.AddForce (0, 0, forwardForce * Time.deltaTime);
        if (Input.GetKey ("d")) {
            rb.AddForce (sidewayForce * Time.deltaTime, 0, 0,ForceMode.VelocityChange);

        }
        if (Input.GetKey ("a")) {
            rb.AddForce (-sidewayForce * Time.deltaTime, 0, 0,ForceMode.VelocityChange);

        }
        if (rb.position.y < -1f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }
}