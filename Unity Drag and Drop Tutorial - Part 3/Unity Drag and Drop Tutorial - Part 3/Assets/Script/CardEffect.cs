﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardEffect : MonoBehaviour
{
    public static void Card1()
    {
        //deal 5damage
        int damage = 5;
        Player.health -= damage;
    }
    //看需求打卡能不能  能return true  不能 false
    public static bool CardCheck(string card)
    {
        switch (card)
        {
            case "Card":

                return true;
            case "Card1":

                return true;
            case "Card2":

                return true;
            case "Card3":

                return true;

            case "Card4":

            default:
                return false;


        }
    }
    public static void CardDefine(string card)
    {
        switch (card)
        {
            case "Card":
                Card1();
                break;
            case "Card1":
                Card1();
                break;
            case "Card2":
                Card1();
                break;
            case "Card3":
                Card1();
                break;

            case "Card4":

            default:
                return;


        }
        //update player status to database
        SaveSystem.SavePlayer();
    }
    public void DrawCard()
    {


        GameObject CardDeck = GameObject.Find("CardDeck");
        GameObject CardUsed = GameObject.Find("CardUsed");
        int cardTotal = CardDeck.transform.childCount;
        int cardDraw = Random.Range(0, cardTotal);
        //如果抽卡區還有卡
        if (cardTotal != 0)
        {

            //把卡從CardDeck移動到Hand
            Transform Card = CardDeck.transform.GetChild(cardDraw);
            Card.transform.SetParent(GameObject.Find("Hand").GetComponent<Transform>());
        }
        //抽卡區沒卡了
        else
        {
            
            //把卡從棄牌堆移動到抽卡區
             while (CardUsed.transform.childCount!=0) {
                Transform Card = CardUsed.transform.GetChild(0);
                Card.transform.SetParent(GameObject.Find("CardDeck").GetComponent<Transform>());
               
            }
            //沒有卡片在抽卡區
            if (CardDeck.transform.childCount == 0) {
                return;
            }
            //再抽一張牌
            DrawCard();
        }

    }
}
