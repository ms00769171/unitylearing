﻿using Proyecto26;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //預設
    public static string playerID = "0";
    public static string playerName = "0";
    public static int health = 100;
    public static int dice1 = 0;
    public static int dice2 = 0;
    public static int dice3 = 0;
    public static List<int> diceCanUse = new List<int>();
    public static List<int> cardCanUse = new List<int>();
    public static bool ready = false;
    public static string matchID = "0";


    public void GetDiceValue()
    {
        dice1 = GameObject.Find("Dice1").GetComponent<Dice>().dicepoint;
        dice2 = GameObject.Find("Dice2").GetComponent<Dice>().dicepoint;
        dice3 = GameObject.Find("Dice3").GetComponent<Dice>().dicepoint;
        if (!(dice1 == -1 || dice2 == -1 || dice3 == -1))
        {
            diceCanUse.Add(dice1);
            diceCanUse.Add(dice2);
            diceCanUse.Add(dice3);

            //SavePlayer
            SaveSystem.SavePlayer();
        }
    }
    public static string GetUniqueID()
    {
        string[] split = System.DateTime.Now.TimeOfDay.ToString().Split(new Char[] { ':', '.' });
        string id = "";
        for (int i = 0; i < split.Length; i++)
        {
            id += split[i];
        }
        return id;
    }




}
