﻿using Proyecto26;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public string playerID;
    public string playerName;
    public int health;
    public int dice1;
    public int dice2;
    public int dice3;
    public List<int> diceCanUse = new List<int>();
    public List<int> cardCanUse = new List<int>();
    public bool ready;
    public string matchID;

    //建構子
    public PlayerData()
    {
        playerID = Player.playerID;
        playerName = Player.playerName;
        health = Player.health;
        dice1 = Player.dice1;
        dice2 = Player.dice2;
        dice3 = Player.dice3;
        diceCanUse = Player.diceCanUse;
        cardCanUse = Player.cardCanUse;
        ready = Player.ready;
        matchID = Player.matchID;
    }




}
