﻿
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Proyecto26;

public class SaveSystem : MonoBehaviour
{
    public static void SavePlayer()
    {
        //去data  data會讀取player
        PlayerData data = new PlayerData();

        RestClient.Put("https://cardlice.firebaseio.com/Players/" + Player.playerID + ".json", data);
    }
    public static void LoadPlayer()
    {
        //去改player
        RestClient.Get<PlayerData>("https://cardlice.firebaseio.com/Players/" + Player.playerID + ".json").Then(response =>
        {
            Player.playerName = response.playerName;
            Player.playerID = response.playerID;
            Player.playerName = response.playerName;
            Player.health = response.health;
            Player.dice1 = response.dice1;
            Player.dice2 = response.dice2;
            Player.dice3 = response.dice3;
            Player.diceCanUse = response.diceCanUse;
            Player.cardCanUse = response.cardCanUse;
        });

    }


}
