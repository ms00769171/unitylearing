﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour
{
    public int Scene;
    public void SceneSwitcher() {
        SceneManager.LoadScene(Scene);


    }
}
