﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Proyecto26;

public class MatchControl : MonoBehaviour
{

    //private match
    public TMP_InputField passwordInput;
    //create room
    public void JoinPrivateMatch()
    {


        Match.password = passwordInput.text;
        Debug.Log(Match.password);
        //先拉出來看有沒有人
        LoadPrivateMatch();
        //延遲2秒後自己加入(因為讀取需要時間)
        Invoke("Join", 2);
    }
    public static void LoadPrivateMatch()
    {
        RestClient.Get<MatchData>("https://cardlice.firebaseio.com/PrivateMatch/" + Match.password + ".json").Then(response =>
        {
            Match.password = response.password;
            Match.Players = response.Players;
            Debug.Log("loaded" + response.Players.Count);


        });
    }
    public void Join()
    {
        Match.Players.Add(Player.playerID);
        Debug.Log("loading" + Match.Players.Count);
        MatchData data = new MatchData();
        RestClient.Put("https://cardlice.firebaseio.com/PrivateMatch/" + Match.password + ".json", data);
    }
}
