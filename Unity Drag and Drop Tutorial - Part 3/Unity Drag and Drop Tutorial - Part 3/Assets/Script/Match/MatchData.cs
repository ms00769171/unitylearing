﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MatchData
{
    public string password;
    public List<string> Players = new List<string>();
    public int round;
    //建構子
    public MatchData()
    {
        password = Match.password;
        Players = Match.Players;
        round = Match.round;
    }
}

