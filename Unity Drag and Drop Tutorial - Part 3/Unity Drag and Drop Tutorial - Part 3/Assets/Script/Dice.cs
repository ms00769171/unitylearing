﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class Dice : MonoBehaviour
{

    public List<Vector3> directions;
    public List<int> sideValues;
    //Before Roll 
    public bool rollReady = true;
    public Vector3 OriginTransform;


    //rollDice
    public Vector3 RotateAmount;  // degrees per second to rotate in each axis. Set in inspector.
    public int count = 0;
    //RandomVector
    public Vector3 minVector = new Vector3(100, 100, 100);
    public Vector3 maxVector = new Vector3(1000, 1000, 1000);
    //Complete Roll Dice

    public int dicepoint = 0;

    public TextMeshProUGUI textdice = null;

    //Save dice value
   
    



    void Start()
    {
        //Befroe Roll
        OriginTransform = this.transform.localPosition;
        //StartRollDice();

    }

    public void StartRollDice()
    {
        dicepoint = -1;
        //正在值骰子
        if (rollReady ==true)
        InvokeRepeating("rollDice", 0.2f, 0.01f);
    }
    public void rollDice()
    {
        rollReady = false;
        //解除xyz鎖定
        this.GetComponent<Rigidbody>().freezeRotation = false;
        RotateAmount = RandomVector(RotateAmount, minVector, maxVector);
        this.transform.Rotate(RotateAmount);
        count++;
        //骰子投了50次動畫
        if (count == 50)
        {
            CancelInvoke("rollDice");
            //真正的骰子面向

            //取得數值


            count = 0;
            RollDiceComplete();
        }

    }
    public void RollDiceComplete()
    {
        Invoke("BackToPosition", 1);



    }
    //random Vector
    public static Vector3 RandomVector(Vector3 myVector, Vector3 min, Vector3 max)
    {
        myVector = new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
        return myVector;
    }
    //freezerotation for invoke
    public void BackToPosition()
    {

        //Reroll
        dicepoint = DetermineDiceValue();
        if (dicepoint == -1)
        {
            rollReady = true;
            StartRollDice();
        }
        //紀錄數值
        else
        {
            //Debug.Log(this + "Complete Roll Dice");
            SaveDice(this, dicepoint);
            //回到位置
            this.GetComponent<Rigidbody>().freezeRotation = true;

            //xyz position
            this.transform.localPosition = OriginTransform;

            rollReady = true;
            //看是不是投了三顆call by Player
            GameObject.Find("Player").GetComponent<Player>().GetDiceValue();


        }


        //this.GetComponent<RectTransform>().Rotate(new Vector3(0, 0, OriZ));


    }

    public void SaveDice(Dice dice, int value)
    {
        textdice.text = "+"+dicepoint;

    }

    public void LoadDice(Dice dice, int value)
    {

    }
    //determine -1有誤要重投擲
    public int DetermineDiceValue()
    {
        //xyz rotation
        Vector3 Ori = this.GetComponent<Transform>().localEulerAngles;

        //xyz整數值
        for (int i = 0; i <= 2; i++)
        {
            int multiple = 0;
            for (int j = 0; j < 10; j++)
            {
                if (Ori[i] < 30.0f && Ori[i] > -30.0f)
                {
                    Ori[i] = 90 * multiple;
                    break;
                }
                else
                {
                    Ori[i] -= 90;
                    multiple++;
                }
                //沒辦法變成90倍數
                if (Ori[i] <= -30.0f)
                {
                    Ori[i] = Mathf.Round(Ori[i]) + 90 * multiple;
                    break;
                }
            }



        }

        //決定值

        if ((Ori.y == 90 && Ori.z == 90) || (Ori.y == 270 && Ori.z == 270))
        {
            return 1;
        }
        else if ((Ori.y == 90 && Ori.z == 0) || (Ori.y == 270 && Ori.z == 180))
        {
            return 2;
        }
        else if ((Ori.x == 0 && Ori.y == 0) || (Ori.x == 180 && Ori.y == 180))
        {
            return 3;
        }
        else if ((Ori.x == 0 && Ori.y == 180) || (Ori.x == 180 && Ori.y == 0))
        {
            return 4;
        }
        else if ((Ori.y == 270 && Ori.z == 0) || (Ori.y == 90 && Ori.z == 180))
        {
            return 5;
        }
        else if ((Ori.y == 270 && Ori.z == 90) || (Ori.y == 90 && Ori.z == 270))
        {
            return 6;
        }
        //決定不出來return-1
        else
        {
            return -1;
        }
    }


}