﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public Transform parentToReturnTo = null;
    public Transform placeholderParent = null;
    public Vector3 screenPoint;


    GameObject placeholder = null;

    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("OnBeginDrag");

        placeholder = new GameObject();
        placeholder.transform.SetParent(this.transform.parent);
        LayoutElement le = placeholder.AddComponent<LayoutElement>();
        le.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
        le.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;
        le.flexibleWidth = 0;
        le.flexibleHeight = 0;

        placeholder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());

        parentToReturnTo = this.transform.parent;
        placeholderParent = parentToReturnTo;
        this.transform.SetParent(this.transform.parent.parent);

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {


        //fix frag時看不到
        screenPoint = eventData.position;
        screenPoint.z = 10.0f; //distance of the plane from the camera
        this.transform.position = Camera.main.ScreenToWorldPoint(screenPoint);

        if (placeholder.transform.parent != placeholderParent)
            placeholder.transform.SetParent(placeholderParent);

        int newSiblingIndex = placeholderParent.childCount;

        for (int i = 0; i < placeholderParent.childCount; i++)
        {
            if (this.transform.position.x < placeholderParent.GetChild(i).position.x)
            {

                newSiblingIndex = i;

                if (placeholder.transform.GetSiblingIndex() < newSiblingIndex)
                    newSiblingIndex--;

                break;
            }
        }

        placeholder.transform.SetSiblingIndex(newSiblingIndex);

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEndDrag");
        //drop on dice table destory
        if (parentToReturnTo == GameObject.Find("DiceTable").GetComponent<Transform>())
        {
            //骰子準備好沒
            if (GameObject.Find("Dice1").GetComponent<Dice>().rollReady == true && GameObject.Find("Dice2").GetComponent<Dice>().rollReady == true && GameObject.Find("Dice3").GetComponent<Dice>().rollReady == true)
            {
                //放到棄牌區
                this.transform.SetParent(GameObject.Find("CardUsed").GetComponent<Transform>());
                //呼叫投骰子
                GameObject.Find("Dice1").GetComponent<Dice>().StartRollDice();
                GameObject.Find("Dice2").GetComponent<Dice>().StartRollDice();
                GameObject.Find("Dice3").GetComponent<Dice>().StartRollDice();
            }
            else
            {
                //骰子還沒準備好  回到手排
                this.transform.SetParent(GameObject.Find("Hand").GetComponent<Transform>());
            }
        }
        //放到牌桌
        else if (parentToReturnTo == GameObject.Find("Tabletop").GetComponent<Transform>())
        {
            //看能不能發動效果
            if (CardEffect.CardCheck(this.gameObject.name))
            {
                //發動效果並放到棄牌區
                CardEffect.CardDefine(this.gameObject.name);
                this.transform.SetParent(GameObject.Find("CardUsed").GetComponent<Transform>());
                //Destroy(this.gameObject);
            }
            else
            {
                //回到手排
                this.transform.SetParent(GameObject.Find("Hand").GetComponent<Transform>());
            }

        }
        
        else
        {
            this.transform.SetParent(parentToReturnTo);
        }
        this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
        GetComponent<CanvasGroup>().blocksRaycasts = true;


        Destroy(placeholder);
    }



}
